//я описал 6 функции (правил), которые возвращают ID клеток куда выбранная фигура может переместиться
//возможно алгоритмы не оптимальны, решил в гугле не искать))

var doc = document,
    contentArea = doc.getElementById('contentArea'),
    board = createElem('div', 'id', 'board', 'style', 'display: flex; border: 1px solid black'),
    figures = createElem('div', 'id', 'figures', 'style', 'display: flex; align-items: flex-start;'),
    king = createElem('div', 'id', 'king', 'innerHTML', '&#x2654', 'style', 'font-size: 80px;'),
    queen = createElem('div', 'id', 'queen', 'innerHTML', '&#x2655', 'style', 'font-size: 80px;'),
    rook = createElem('div', 'id', 'rook', 'innerHTML', '&#x2656', 'style', 'font-size: 80px;'),
    bishop = createElem('div', 'id', 'bishop', 'innerHTML', '&#x2657', 'style', 'font-size: 80px;'),
    knight = createElem('div', 'id', 'knight', 'innerHTML', '&#x2658', 'style', 'font-size: 80px;'),
    pawn = createElem('div', 'id', 'pawn', 'innerHTML', '&#x2659', 'style', 'font-size: 80px;'),
    textArea = createElem('div', 'id', 'textArea', 'style', 'font-size: 100px');
alf = {
    1: 'a',
    2: 'b',
    3: 'c',
    4: 'd',
    5: 'e',
    6: 'f',
    7: 'g',
    8: 'h'
};
contentArea.style.display = 'flex';
contentArea.style.alignItems = 'flex-start';
contentArea.appendChild(board);
contentArea.appendChild(figures);
contentArea.appendChild(textArea);
figures.appendChild(king);
figures.appendChild(queen);
figures.appendChild(rook);
figures.appendChild(bishop);
figures.appendChild(knight);
figures.appendChild(pawn);

figures.addEventListener('click', function(e) {
    textArea.innerHTML = e.target.id;
})

function fillBoard() {
    var row = [];
    for (var i = 1; i <= 8; i++) {
        row[i] = document.createElement('div');
        row[i].style.float = 'left';
        for (var j = 8; j >= 1; j--) {
            if ((i + j) % 2) {
                row[i].appendChild(createCell(j + alf[i], 'white'));
            } else {
                row[i].appendChild(createCell(j + alf[i], 'black'));
            }
        }
        board.appendChild(row[i]);
    }
}
fillBoard();

function createCell(id, color) {
    var cell = createElem('div', 'id', id, 'style', 'width: 50px; height: 50px; margin: 0; border: 1px solid black; background-color: ' + color + ';');
    return cell;
}

function createElem(parent) {
    var newElem = document.createElement(parent);
    for (i = 1; i < arguments.length; i = i + 2) {
        newElem[arguments[i]] = arguments[i + 1];
    }
    return newElem;
}

board.addEventListener('click', function(e) {
    board.innerHTML = '';
    fillBoard();
    doc.getElementById(e.target.id).style.backgroundColor = 'red';
    e.target.style.backgroundColor = 'red';
    var test;
    if (textArea.innerHTML != '') {
        test = figures[textArea.innerHTML + 'Rule'](e.target.id);
        console.log(test);
        for (var p = 0; p < test.length; p++) {
            doc.getElementById(test[p]).style.backgroundColor = 'green';
        }
    }
})

figures.pawnRule = function(id) {
    var i = id[0],
        d = id[1];
    i = i >= 8 ? i : ++i;
    return i + d == id ? false : [i + d];
}

figures.rookRule = function(id) {
    var i = id[0],
        d = id[1],
        answer = [];
    for (var a = 1; a <= 8; a++) {
        if (d != alf[a]) {
            answer.push(i + alf[a]);
        }
        if (a != i) {
            answer.push(a + d);
        }
    }
    return answer;
}

figures.knightRule = function(id) {
    var i = id[0],
        d = id[1],
        answer = [];
    for (key in alf) {
        if (alf[key] == d) {
            d = key;
        }
    }
    if (i <= 8 - 2) {
        if (d <= 8 - 1) {
            answer.push(+i + 2 + alf[+d + 1]);
        }
        if (d >= 1 + 1) {
            answer.push(+i + 2 + alf[+d - 1]);
        }
    }
    if (i >= 1 + 2) {
        if (d <= 8 - 1) {
            answer.push(+i - 2 + alf[+d + 1]);
        }
        if (d >= 1 + 1) {
            answer.push(+i - 2 + alf[+d - 1]);
        }
    }
    if (d <= 8 - 2) {
        if (i <= 8 - 1) {
            answer.push(+i + 1 + alf[+d + 2]);
        }
        if (i >= 1 + 1) {
            answer.push(+i - 1 + alf[+d + 2]);
        }
    }
    if (d >= 1 + 2) {
        if (i <= 8 - 1) {
            answer.push(+i + 1 + alf[+d - 2]);
        }
        if (i >= 1 + 1) {
            answer.push(+i - 1 + alf[+d - 2]);
        }
    }
    return answer;
}

figures.bishopRule = function(id) {
    var i = id[0],
        d = id[1],
        answer = [];
    for (key in alf) {
        if (alf[key] == d) {
            d = key;
        }
    }

    function a(i, d) {
        if (i <= 8 - 1 && d >= 1 + 1) {
            answer.push(+i + 1 + alf[+d - 1]);
            a(+i + 1, +d - 1)
        }
    }

    function b(i, d) {
        if (i <= 8 - 1 && d <= 8 - 1) {
            answer.push(+i + 1 + alf[+d + 1]);
            b(+i + 1, +d + 1);
        }
    }

    function aa(i, d) {
        if (i >= 1 + 1 && d >= 1 + 1) {
            answer.push(+i - 1 + alf[+d - 1]);
            aa(+i - 1, +d - 1);
        }
    }

    function bb(i, d) {
        if (i >= 1 + 1 && d <= 8 - 1) {
            answer.push(+i - 1 + alf[+d + 1]);
            bb(+i - 1, +d + 1);
        }
    }
    a(i, d);
    b(i, d);
    aa(i, d);
    bb(i, d);
    return answer;
}

figures.queenRule = function(id) {
    return this.rookRule(id).concat(this.bishopRule(id));
}

figures.kingRule = function(id) {
    var i = id[0],
        d = id[1],
        answer = [];
    for (key in alf) {
        if (alf[key] == d) {
            d = key;
        }
    }
    if (i <= 8 - 1) {
        for (var a = +d - 1; a <= +d + 1; a++) {
            if (a >= 1 && a <= 8) {
                answer.push(+i + 1 + alf[a]);
            }
        }
    }
    if (i >= 1 + 1) {
        for (var a = +d - 1; a <= +d + 1; a++) {
            if (a >= 1 && a <= 8) {
                answer.push(+i - 1 + alf[a]);
            }
        }
    }
    if (d <= 8 - 1) {
        answer.push(+i + alf[+d + 1]);
    }
    if (d >= 1 + 1) {
        answer.push(+i + alf[+d - 1]);
    }
    return answer;
}