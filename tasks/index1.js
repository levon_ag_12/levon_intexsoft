var doc = document,
    contentArea = doc.getElementById('contentArea'),
    inputForm = createElem('div', 'id', 'inputForm'),
    showForm = createElem('div', 'id', 'showForm'),
    input = createElem('input', 'id', 'input', 'type', 'number', 'min', 1, 'max', 30),
    buttonSend = createElem('input', 'id', 'buttonSend', 'type', 'button', 'value', 'Send'),
    buttonClear = createElem('input', 'id', 'buttonClear', 'type', 'button', 'value', 'Clear'),
    selectFigure = createElem('div', 'id', 'selectFigure', 'style', 'display: flex; justify-content: space-between; width: 210px;');

selectFigure.addEventListener("click", function(e) {
    var elems = this.getElementsByTagName('div');
    buttonSend.cash = e.target == elems[0] ? 'square' :
        e.target == elems[1] ? 'circle' :
        e.target == elems[2] ? 'squareRight' :
        e.target == elems[3] ? 'squareLeft' : '';
});

putElems(selectFigure, createSquare(), createCircle(), createSquareRight(), createSquareLeft());
putElems(inputForm, putElems(createElem('div', 'id', 'blocks'), input, buttonSend, buttonClear), selectFigure);

putElems(contentArea, inputForm, showForm);
buttonSend.onclick = function() {
    showForm.innerHTML = '';
    input.value = input.value > 30 ? 30 :
        input.value < 0 ? 0 :
        input.value;
    if (buttonSend.cash == '') {
        alert('choose figure!')
    } else {
        putElems(showForm, drowShowBlock(buttonSend.cash, input.value));
    }
}
buttonClear.onclick = function() {
    showForm.innerHTML = '';
}

function putElems(parent) {
    for (i = 1; i < arguments.length; i++) {
        parent.appendChild(arguments[i]);
    }
    return parent;
}

function createElem(parent) {
    var newElem = document.createElement(parent);
    for (i = 1; i < arguments.length; i = i + 2) {
        newElem[arguments[i]] = arguments[i + 1];
    }
    return newElem;
}

function createCircle(backgroundColor = 'white', border = '1px solid black', size = '50px') {
    var circle = createSquare(backgroundColor, border, size);
    circle.style.borderRadius = parseInt(size) / 2 + 'px';
    return circle;
}

function createSquare(backgroundColor = 'white', border = '1px solid black', size = '50px') {
    var square = createElem('div');
    square.style.backgroundColor = backgroundColor;
    square.style.margin = '3px';
    square.style.border = border;
    square.style.width = size;
    square.style.height = size;
    return square;
}

function createSquareRight(backgroundColor = 'white', border = '1px solid black', size = '50px') {
    var squareRight = createSquare(backgroundColor, border, size);
    squareRight.style.borderTopLeftRadius = parseInt(size) / 2 + 'px';
    squareRight.style.borderBottomRightRadius = parseInt(size) / 2 + 'px';
    return squareRight;
}

function createSquareLeft(backgroundColor = 'white', border = '1px solid black', size = '50px') {
    var squareRight = createSquare(backgroundColor, border, size);
    squareRight.style.borderTopRightRadius = parseInt(size) / 2 + 'px';
    squareRight.style.borderBottomLeftRadius = parseInt(size) / 2 + 'px';
    return squareRight;
}

function drowShowBlock(figure, size) {
    var blockWithFigures = document.createElement('div');
    var row = [];
    for (var i = 0; i < size; i++) {
        row[i] = document.createElement('div');
        row[i].style.float = 'left';
        for (var j = 0; j < size; j++) {
            row[i].appendChild(figureCSS(figure));
        }
        blockWithFigures.appendChild(row[i]);
    }

    function figureCSS(figure) {
        var elem;
        switch (figure) {
            case 'circle':
                elem = createCircle(randomColor(), 0, '15px');
                break;
            case 'square':
                elem = createSquare(randomColor(), 0, '15px');
                break;
            case 'squareRight':
                elem = createSquareRight(randomColor(), 0, '15px');
                break;
            case 'squareLeft':
                elem = createSquareLeft(randomColor(), 0, '15px');
                break;
            default:
                elem = '';
                break;
        }
        return elem;
    }
    return blockWithFigures;
}

function randomColor() {
    var color = '#';
    for (var i = 0; i < 3; i++) {
        var r = parseInt(Math.random() * 255).toString(16);
        r = ('' + r).length == 1 ? '0' + r : r;
        color = color + r;
    }
    return color;
}