
console.log('qwe'-'ewq');   //NaN
var variable = true + 'test';
console.log(variable);      //truetest
console.log('123' + undefined); //123undefined
console.log(Number('1234'));
console.log(+'33424');
console.log(+undefined);    //Nan
console.log(+null); //0
console.log(+true); //1
console.log(+false);//0
console.log(+'123 32'); //NaN
console.log(+'  \t4324\n\n   ');    //4324
console.log(+'qwerty'); //NaN
console.log(+'\n\t');   //0
console.log(+1);    //1
console.log(!!1);   //true
console.log(!!124); //true
console.log(!!-24); //true
console.log(!!0,'\n');   //false
console.log(undefined>0);   //false
console.log(undefined<0);   //false
console.log(undefined=0);   //0
console.log(undefined==0);  //false
console.log(undefined>=0);  //false
console.log(undefined<=0,'\n');  //false
console.log(null>0);   //false
console.log(null<0);   //false
console.log(null==0);  //false
console.log(null>=0);  //true
console.log(null<=0);  //true
console.log(undefined==null);//true
console.log(undefined>=null);//false
console.log(undefined<=null,'\n');//false
console.log(!!undefined,!!null,!!' ');  //false false true
console.log(!!3 || !!0);//true
console.log(!!3 != !!4);//false
console.log(13%2,13/2);//1  6.5
console.log(-false);//-0
console.log(-true);//-1
var qw = 5;
console.log(qw++,qw,++qw);  //5    6    7
console.log(~132,~'qwer',~-24);  //-133  -1   23