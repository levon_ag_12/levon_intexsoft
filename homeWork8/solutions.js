//функция проверяет есть ли хотя бы одно значение 'value' в массиве 'arr'
function hasMatch(value, arr){
    var bool = false;
    for (var i = 0; i < arr.length; i++){
        if (arr[i] == value){            
            bool = true;
        }
    }
    return bool;
}
//функция для копирования библиотеки 
function copyLibrary(input){
    var output = [];
    for (var i = 0; i < input.length; i++){
        output[i] = {};    
        for (var key in input[i]){
            output[i][key] = input[i][key];
        }
    }
    return output;
}

function query(collection) {
    var newLib = copyLibrary(collection);
    var arr = [];
    if (arguments.length > 1){
        for (var i = 1; i < arguments.length; i++){
            if (arguments[i].name == 'filterIn'){
                newLib = arguments[i].action(newLib);
            } else if (arguments[i].name == 'select'){
                arr = arr.concat(arguments[i]);
            }
        }
        if (arr.length > 0){
            for (var i = 0; i < arr.length; i++){
                newLib = arr[i].action(newLib);
            }
        }
    }
    return newLib;
}
function select() {
    var args = arguments;
    return {
        name: 'select',
        action: function(library){
            for (var i = 0; i < library.length; i++){
                for (var key in library[i]){
                    if (!hasMatch(key, args)){
                        delete library[i][key];
                    }
                }
            }
            return library;
        }
    }
}
function filterIn(property, values) {
    return {
        name: 'filterIn',
        action: function(library){
            for (var i = 0; i < library.length; i++){
                if (!hasMatch(library[i][property], values)){
                    library.splice(i,1);
                    i--;
                }
            }
            return library;
        }
    }
}

module.exports = {
    timeShift: function(date) {
        obj = {
            iDate: new Date(date),
            add: function(value, name){
                if (name == 'hours'){
                    this.iDate.setHours(this.iDate.getHours() + value);
                } else if (name == 'minutes'){
                    this.iDate.setMinutes(this.iDate.getMinutes() + value);
                } else if (name == 'years') {
                    this.iDate.setFullYear(this.iDate.getFullYear() + value);
                } else if (name == 'days') {
                    this.iDate.setDate(this.iDate.getDate() + value);
                } else if (name == 'months') {
                    this.iDate.setMonth(this.iDate.getMonth() + value);
                }
                return this;
            },
            subtract: function(value, name){ 
                this.add(-value, name);
                return this;
            }
        };
        Object.defineProperty(obj,'value',{
            get: function(){
                return (('000' + this.iDate.getFullYear()).slice(-4) + '-' +
                ('0' + (this.iDate.getMonth() + 1)).slice(-2) + '-' +          // month [0 .. 11]
                ('0' + this.iDate.getDate()).slice(-2) + ' ' +
                ('0' + this.iDate.getHours()).slice(-2) + ':' +
                ('0' + this.iDate.getMinutes()).slice(-2));
            },
        })
        return obj;
    },
    lib: {
        query: query,
        select: select,
        filterIn: filterIn
    }    
};
