function isInteger(n) {
    return n === +n && n === (n|0);
}
function getDayDeclension(a){
    var answer = '';
    if(isInteger(a) && a >= 0){
        while(a>=100){
            a=a-100;
        }
        if (a>=20 && a<=99){
            a = a % 10;
        }
        switch(a){
            case 1:
                answer = 'День'; break;
            case 2:
            case 3:
            case 4:
                answer = 'Дня'; break;
            default:
                answer = 'Дней'; break;
        }
    }
    return answer;
}