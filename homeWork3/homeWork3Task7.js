function isPointInCircle(x,y){
    if (Math.pow((x-3),2) + Math.pow((y-5),2) <= Math.pow(4,2)){
        return true;
    }else{
        return false;
    }
}

function isPointInQuadrilateral(x,y){
   if(y <= 4*x/7+4 && y <=3-0.6*x && y >= 0.4*x-2 && y >= -1.5*x-12){
       return true;
   }else{
       return false;
   }
}