function getSum(a){
    var sum = 0;
    for(var i = 0; i <= Math.abs(a); i++){
        sum = sum + i;
    }
    return (Math.sign(a) * sum);
}

function getSumRecursion(a){
    if (a == 1){
        return 1;
    }
    return a + getSumRecursion(a-1);
}

function getFactorial (a){
    if (a != 0){
        return a * getFactorial(a-1)
    } else {
        return 1;
    }
}