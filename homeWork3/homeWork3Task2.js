function isInteger(n) {
    return n === +n && n === (n|0);
}
function addMinutes(a,b,c){
    var answer = '';
    if (isInteger(a) && isInteger(b) && isInteger(c) && a<=23 && a>=0 && b<=59 && b>=0 && c>=0){
        for(b = b + c; b > 59;b=b-60){
            a++;
            if(a==24){
                a = a - 24;
            }
        }
        answer = ('0' + a).slice(-2) + ':' + ('0' + b).slice(-2);
    } 
    return answer;
}