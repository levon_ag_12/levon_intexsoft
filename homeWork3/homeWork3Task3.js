const getSeasonAlias = alias => alias === 12 ? 0 : alias;

const seasonMap = new Map([[0, 'Winter'], [1, 'Spring'], [2, 'Summer'], [3, 'Autumn']]);

const getSeason = value => value >= 1 && value <= 12 ? seasonMap.get(Math.floor(getSeasonAlias(value)/3)) : 'Month error';