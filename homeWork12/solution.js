module.exports = Collection;
function Collection() {
    this.items = [];
}
Collection.prototype.append = function(element) {
    if (element instanceof Collection){
        this.items = this.items.concat(element.items);
    } else {
        this.items.push(element);
    }
};
Collection.prototype.removeAt = function (index) {
    if (this.items[index-1]){
        this.items.splice(index-1, 1);
        return true;   
    } else {
        return false;
    }
};
Collection.prototype.values = function () {
    return this.items;
};
Collection.prototype.count = function () {
    return this.items.length;
};
Collection.prototype.at = function (index) {
    if (this.items[index-1]){
        return this.items[index-1];   
    } else {
        return null;
    }
};
Collection.from = function (item) {
    var collection = new Collection();
    if (typeof(item) != 'object'){
        addElementsToCollection(arguments);
    } else {
        addElementsToCollection(item);
    }
    return collection;
    
    function addElementsToCollection(setOfElements){
        for (var i = 0; i < setOfElements.length; i++){
            collection.append(setOfElements[i]);
        }
    }
};