//Подсказка: сюда можно складывать записи адресной книги.
var phoneBook = [];

//Здесь можно объявить переменные и функции, которые понядобятся вам для работы ваших функций
function addPhone(input){
    var arr = input.split(' ');
    if(phoneBook.length == 0){
        phoneBook[0] = arr[0] + ': ' + arr[1];
    }else{
        for (var i = 0; i < phoneBook.length; i++){
            if (phoneBook[i].indexOf(arr[0]) == 0){
                phoneBook[i] = phoneBook[i] + ',' + arr[1];
                i = phoneBook.length;
            }else if (i == phoneBook.length-1){
                phoneBook[i+1] = arr[0] + ': ' + arr[1];
                i++;
            }
        }
    }
}

function removePhone(input){
    for (var i = 0; i < phoneBook.length; i++){
        if (phoneBook[i].indexOf(input) > 0){
            var a;
            a = phoneBook[i].indexOf(input);
            phoneBook[i] = phoneBook[i].slice(0,a) + phoneBook[i].slice(a+input.length+1);
            return true;
        }
    }
}

function showBook(){
    var arr = phoneBook.filter(function(item){
        return ((item.indexOf(':')+2 < item.length) && (item.indexOf(':')+1 < item.length))
    });
    for (var i = 0; i < arr.length; i++){
        if (arr[i].indexOf(',')>0){
            arr[i] = arr[i].split(',').join(', ');
        }
    }
    return arr.sort();
}

module.exports = {
    getWords: function(sentence){
        return (sentence.split(' ').filter(function(item){
            return (item.indexOf('#') == 0);
        }).map(function(item){
            return (item.slice(1));
        }))
    },
    normalizeWords: function(words){
        var arr = words.map(function(item){
            return item.toLowerCase();
        })
        var obj = {};
        for (var i = 0; i < arr.length; i++){
            obj[arr[i]] = true;
        }
        var answer = Object.keys(obj).join(', ');
        answer.length = answer.length - 2;
        return answer;
    },
    addressBook: function(command){
        if (command.indexOf('ADD') == 0){
            addPhone(command.substring(4));
        }else if(command.indexOf('REMOVE_PHONE') == 0){
            return removePhone(command.substring(13));
        }else if(command.indexOf('SHOW') == 0){
            return showBook();
        }
    }
}
